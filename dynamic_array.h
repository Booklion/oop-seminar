#pragma once



typedef struct {
	unsigned int cap;
	unsigned int len;
	void** data; // DynamicArray of void*
}DynamicArray;

DynamicArray* create(unsigned int initialCap);

void insert(DynamicArray* a, void* elem);

void deleteLast(DynamicArray* a);

void display(DynamicArray* a, void (*displayElement)(void*));

void release(DynamicArray* a);

int search(DynamicArray* a, bool (*condition)(void*));
