#include "dynamic_array.h"
#include <iostream>
using namespace std;

DynamicArray* create(unsigned int initialCap) {
	DynamicArray* result = new DynamicArray;
	result->len = 0;
	result->cap = initialCap;
	result->data = new void*[initialCap];
	return result;
}

void insert(DynamicArray* a, void* elem) {
	if (a->len == a->cap) {
		a->cap = a->cap * 2;
		void** newArray = new void*[a->cap];
		for (unsigned int i = 0; i < a->len; i++) {
			newArray[i] = a->data[i];
		}
		delete[] a->data;
		a->data = nullptr;
		a->data = newArray;
	}
	a->data[a->len++] = elem;
}

void display(DynamicArray* a, void (*displayElement)(void*)) {
	for (unsigned int i = 0; i < a->len; i++) {
		displayElement(a->data[i]);
	}
	cout << endl;
}

void release(DynamicArray* a) {
	delete[] a->data;
	delete a;
	a = nullptr;
}
int search(DynamicArray* a, bool(*condition)(void*)) {
	for (unsigned int i = 0; i < a->len; i++) {
		if (condition(a->data[i])) {
			return i;
		}
	}
	return -1;
}
/*
void deleteLast() {

}
*/